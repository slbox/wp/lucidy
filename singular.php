<?php
namespace Lucidy;
loadLib('EntryPost');
$entry = new Entry\Post();
?>

<?php header() ?>

<main>
	<?php
	render('entry/header', [
		'entry' => $entry
	]);
	render('entry/body', [
		'entry' => $entry
	]);
	render('entry/author', [
		'author' => $entry->author
	]);
	render('entry/suggestion', [
		// 
	]);
	// render('widgetArea', [
	// 	'id'	=> 'entry-ads-top-cmt',
	// 	'label'	=> 'Entry Advertisement: Top of Comment Thread',
	// 	'desc'	=> 'Landscape, max height 100px',
	// 	'place'	=> 'inserted', # TODO left, right, inserted, (flex) end, (flex) start
	// ]);
	render('entry/commentThread', [
		// 
	])
	?>
</main>

<?php footer() ?>