<?php
$slide = [
	'query'	=> [
		'nopaging'			=> true,
		'post__in'			=> get_option('sticky_posts'),
		'orderby'			=> 'modified',
		'posts_per_page'	=> 5
	]
];

$card = [
	'query'	=> [
		'post__not_in'		=> get_option('sticky_posts')
	]
]
?>

<?php Lucidy\header('front') ?>

<?php if( is_home() && is_sticky() ): ?>
	<section id=highlight>
		<?php Lucidy\render('slideshow', $slide) ?>
	</section>
<?php endif ?>

<main>
	<?php Lucidy\render('cardbox', $card) ?>
</main>

<?php Lucidy\footer('detail') ?>