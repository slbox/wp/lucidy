(function () {
	'use strict';

	class Button {
	  constructor(id) {
	    this.core = document.querySelector(id);
	  }

	  toggle(active) {
	    const toggle = 'invisible';
	    if (active) this.core.classList.remove(toggle);else this.core.classList.add(toggle);
	  }

	  onclick() {}

	}

	class btnUP extends Button {
	  constructor() {
	    super('#UP');
	  }

	  goUp(e) {
	    e.preventDefault();
	    window.scrollTo(0, 0);
	  }

	  onclick() {
	    this.core.addEventListener('click', this.goUp);
	  }

	}

	class btnMagnifier extends Button {
	  constructor() {
	    super('#UP');
	  }

	}

	const UP = new btnUP();
	const magnifier = new btnMagnifier();

	class Card {
	  constructor(card) {
	    if (typeof card === 'string') {
	      this.name = card;
	      this.self = document.querySelector(card);
	    } else if (card instanceof HTMLElement) {
	      this.self = card;
	      this.name = this.primeClass(card.className);
	    }

	    this.id = this.self.id;
	    this.thumb = this.child('thumbnail');
	    this.body = this.child('body');
	  }

	  child(el) {
	    return this.self.querySelector(`.${this.name}-${el}`);
	  }
	  /**
	   * Get the main class of the card
	   * @param {String} lsClass from Element.className
	   */


	  primeClass(lsClass = '') {
	    // TODO optimize the case 'class class-1    class-1-prop'
	    const list = lsClass.trim().split(' ');

	    for (const item of list) {
	      if (item.indexOf('--') === -1) return item;
	    }
	  }

	  fixSize() {
	    // TOFIX do not resize small size (--small) or small viewport
	    const verticalMarginOf = el => {
	      const style = window.getComputedStyle(el);
	      const top = parseFloat(style.marginTop);
	      const bottom = parseFloat(style.marginBottom);
	      return top + bottom;
	    };

	    const fullHeightOf = name => {
	      const el = this.child(name);
	      return parseFloat(verticalMarginOf(el) + el.clientHeight);
	    };

	    const title = fullHeightOf('title');
	    const content = fullHeightOf('content');
	    const date = fullHeightOf('date');
	    const bodyMargin = verticalMarginOf(this.body);
	    this.self.style.height = `${title + content + date + bodyMargin}px`;
	  }

	}

	const displayUPbtn = () => {
	  const cond = window.scrollY > window.innerHeight;
	  UP.toggle(cond);
	};

	UP.onclick();
	window.addEventListener('scroll', displayUPbtn);

	const adjustCardSize = () => {
	  const cards = document.querySelectorAll('.cardbox-card');
	  cards.forEach(card => {
	    new Card(card).fixSize();
	  });
	};

	window.addEventListener('resize', adjustCardSize);

	const getRestData = () => {};

	window.addEventListener('click', getRestData);
	window.addEventListener('load', () => {
	  displayUPbtn();
	  adjustCardSize();
	});

}());
//# sourceMappingURL=bundle.js.map
