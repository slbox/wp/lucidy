<?php
namespace Lucidy;

class Hook {
	/**
	 * Private methods
	 */
	private function siteFeatures() {
		add_theme_support('title-tag');
		add_theme_support('automatic-feed-links');

		add_theme_support('custom-logo', [
			'flex-width'	=> true,
			'flex-height'	=> true
		]);
	
		register_nav_menus([
			'menu_col' => lutra('Menu Column', 'Menu position name'),
			'menu_list' => lutra('Menu List', 'Menu position name'),
			'menu_bar' => lutra('Menu Bar', 'Menu position name')
		]);
	}

	private function postFeatures() {
		add_theme_support('post-thumbnails');
	}

	/**
	 * Hook Functions
	 */
	/* Filter */
	public function changeSeparator() {
		// TODO: add option to change separator
		return '🌼';
	}

	/* Action */
	public function defaultSetting() {
		update_option('thumbnail_size_w', 555);
	}

	public function registerAssets() {
		$assets = get_template_directory_uri().'/asset';
		$style = "${assets}/lucidy.css";
		$font = "${assets}/fonts.css";
		$script = "${assets}/bundle.js";

		// Register stylesheet & script
		wp_register_style('lucidy', $style);
		wp_register_style('lucidy-font', $font);
		wp_register_script('lucidy-script', $script, '', '', true);

		// Load stylesheet & script
		wp_enqueue_style('lucidy');
		wp_enqueue_style('lucidy-font');
		wp_enqueue_script('lucidy-script');
	}

	public function hideProtectedPost() {
		// Alternative:
		// developer.wordpress.org/reference/classes/wp_query/#password-parameters
		if ( !is_singular() && !is_admin() ):
			add_filter('posts_where', function($where){
				global $wpdb;
				return $where .= " AND {$wpdb->posts}.post_password=''";
			});
		endif;
	}

	public function register() {
		$this->siteFeatures();
		$this->postFeatures();
	}

	function __construct() {
		/* Action hooks */
		add_action('after_setup_theme', [$this, 'register']);
		// add_action('pre_get_posts', [$this, 'hideProtectedPost']);
		add_action('wp_enqueue_scripts', [$this, 'registerAssets']);
		add_action('switch_theme', [$this, 'defaultSetting']);
		
		/* Filter hooks */
		add_filter('document_title_separator', [$this, 'changeSeparator']);
		
		/* Remove WordPress generator meta */
		remove_action('wp_head', 'wp_generator');
	}
}