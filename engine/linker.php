<?php
namespace Lucidy;

class File {
	protected	$name	= '';
	protected	$path	= '';
	private		$data	= null;

	function __construct(string $name, $data=[]) {
		$this->rename($name)->setData($data);
	}

	public function rename(string $name):File {
		$this->name = $name;
		$this->path = trim($name, "/\\").'.php';
		return $this;
	}

	public function setData($data=[]):File {
		$this->data = $data;
		return $this;
	}

	public function load(bool $once=true):bool {
		$located = Security::testPath($this->path);
		if (!$located):
			return false;
		endif;

		// $d is data passed from the parent file
		$d = $this->data;

		if ($once):
			return require_once $located;
		else:
			return require $located;
		endif;
	}
}

class Security {
	/**
	 * Check Directory Traversal Attacks security hole:
	 * - TRUE	: no DTA found
	 * - FALSE	: dangerous, should refuse to load
	 *
	 * @private
	 * @param string $path	
	 * @param string $fixer	This fixed path prevents Directory Traversal Attacks
	 * @return bool
	 */
	static public function noDTA(string $fixer, string $path):bool {
		// developer.wordpress.org/reference/functions/locate_template/#comment-2405
		$temp = $fixer.'/'.$path;
		$exists = file_exists($temp);
		$noDTA = strpos(
			realpath($temp),
			realpath($fixer)
		) === 0;
		return ($exists && $noDTA);
	}

	static public function testPath(string $path) {
		if ( Security::noDTA(STYLESHEETPATH, $path) ):
			return STYLESHEETPATH.'/'.$path;
		elseif ( Security::noDTA(TEMPLATEPATH, $path) ):
			return TEMPLATEPATH.'/'.$path;
		else:
			return false;
		endif;
	}
}

function loader() {}

function loadEngine(string $name):void {
	$file = new File("engine/${name}");
	$file->load();
}

function loadLib(string $name, $data=[], string $default=''):void {
	$lib = new File("lib/${name}", $data);
	if ( !$lib->load() ):
		$lib->rename("lib/${default}")->load();
	endif;
}

// Should I add automatically loader for lib?
function render(string $name, $data=[], string $default=''):void {
	$view = new File("view/${name}", $data);
	if ( !$view->load(false) ):
		$view->rename("view/${default}")->load(false);
	endif;
}

function header(string $type='bar'):void {
	render('html/head');
	if ($type != 'none' || $type != ''):
		render("header/${type}");
	else:
		render("header/bar");
	endif;
}

function footer(string $type='none'):void {
	if ($type != 'none' || $type != '') render("footer/${type}");
	render('html/end');
}