<?php
namespace Lucidy;

function get($array, $index, $default=null) {
	return isset($array[$index]) ? $array[$index] : $default;
}

function loop($query, $item, $data=[]) {
	while( $query->have_posts() ):
		$query->the_post();
		render($item, $data);
	endwhile; wp_reset_postdata();
	# $query->the_post() adds the current post
	#	from the loop to global $post variable
}

function defaultThumb($size='thumbnail') {
	# TODO get URL with a defined size
	$defaultThumbID = get_theme_mod('default_thumbnail');
	return wp_get_attachment_image_url($defaultThumbID, $size);
}