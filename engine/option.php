<?php
namespace Lucidy;

/**
 * Theme Customization
 */
class Option {
	protected $cm; // Customize Manager
	protected $curSect = null;

	protected function __(array $fields):array {
		if ( !empty($fields['label']) ):
			$fields['label'] = lutra(
				$fields['label'],
				'Theme option Label'
			);
		endif;

		if ( !empty($fields['desc']) ):
			$fields['description'] = lutra(
				$fields['desc'],
				'Theme option description'
			);
			unset($fields['desc']);
		endif;

		return $fields;
	}

	protected function filterCtrl(array ...$ctrl):array {
		// Refer to newSect(string $id, array $val)
		$defCtrl['section'] = $this->curSect;
		$merged = array_replace($defCtrl, ...$ctrl);
		return $this->__($merged);
	}
	
	protected function newSect(string $id, array $val):string {
		$val = $this->__($val);
		$this->cm->add_section($id, $val);
		return $this->curSect = $id;
	}
	
	protected function newMedia(string $id, array $ctrl, array $val=[]):void {
		$defCtrl['mime_type'] = 'image';
		$ctrl = $this->filterCtrl($defCtrl, $ctrl);

		$this->cm->add_setting($id, $val);

		$mediaCtrl = new \WP_Customize_Media_Control($this->cm, $id, $ctrl);
		$this->cm->add_control($mediaCtrl);
	}

	protected function newOpt(string $id, array $ctrl, array $val=[]):void {
		$this->cm->add_setting($id, $val);
		$ctrl = $this->filterCtrl($ctrl);
		$this->cm->add_control($id, $ctrl);
	}

	public function general($wp_customize_manager):void {
		$this->cm = $wp_customize_manager;
		
		$this->newSect(
			'lucidy',
			[
				'title' => 'Lucidy theme option',
				'desc' => 'Make Lucidy even more customizable',
				'priority' => 30,
			]
		);

		$this->newMedia(
			'default_thumbnail',
			[
				'label' => 'Set default thumbnail',
				'description' => 'Every card in the theme must have thumbnail. In case writers forget to add it, a default thumbnail will be use.'
			],
			['default'	=> get_template_directory_uri().'/asset/img/Thumbnail.jpg']
		);

		$this->newOpt(
			'max_excerpt_words',
			[
				'label' => 'Max words for auto excerpt.',
				'description' => 'In case writer forget to write excerpt or add <!--more-->, the theme will get a number of words from the post to use as excerpt.'
			],
			['default'	=> 50]
		);

		$this->newOpt(
			'blog_owner',
			[
				'type'		=> 'number',
				'label'		=> 'Blog owner',
				'desc'		=> 'Display blog owner name using User ID'
			],
			[
				'type'		=> 'option',
				'default'	=> 1
			]
		);

		$this->newOpt(
			'theme_credit',
			[
				'type'		=> 'checkbox',
				'label'		=> 'Theme Credit',
				'desc'		=> 'Display credit of theme creator'
			],
			['default'	=> true]
		);
	}

	public function e404($wp_customize_manager):void {
		$this->cm = $wp_customize_manager;
				
		$this->newSect(
			'lucidy404',
			[
				'title' => '404 error page',
				'desc' => 'Let\'s make 404 scary again!',
				'priority' => 30,
			]
		);

		$this->newOpt(
			'title404',
			[
				'type'		=> 'text',
				'label'		=> 'Error 404 title'
			],
			['default'	=> __('Nothing found', 'lucidy')]
		);

		$this->newMedia(
			'thumb404',
			['label' => 'Error 404 thumbnail']
		);

		$this->newOpt(
			'msg404',
			[
				'type'		=> 'text',
				'label'		=> 'Error 404 message'
			],
			['default'	=> __('Found Nothing', 'lucidy')]
		);
	}

	function __construct() {
		add_action('customize_register', [$this, 'general']);
		add_action('customize_register', [$this, 'e404']);
	}
}