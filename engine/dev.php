<?php
if (!function_exists('debug') && WP_DEBUG):
	function debug(...$dump) {
		?><pre><?php var_dump(...$dump) ?></pre><?php
	}
elseif ( !function_exists('debug') ):
	function debug($dump) {}
endif;