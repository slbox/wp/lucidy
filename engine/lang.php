<?php

/**
 * lutra($text, $context)
 * Lucidy translates $test in $context under lucidy domain
 */
function lutra($text, $context = 'No Context') {
	return translate_with_gettext_context($text, $context, 'lucidy');
}