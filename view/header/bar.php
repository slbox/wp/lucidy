<?php
$home = get_bloginfo('url');

?>

<header class='site-header site-header--bar'>
	<a href='<?=$home?>'>
		<h1 class='site-header-item site-title'><?=get_bloginfo('name')?></h1>
	</a>
	<!-- @TODO add ico menu, search button in a same line of site-title -->
</header>