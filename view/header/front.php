<?php
$home = get_bloginfo('url');
$about = home_url('/about/');
?>

<header class='site-header site-header--front'>
	<a href='<?=$home?>'>
		<h1 class='site-title'><?=get_bloginfo('name')?></h1>
	</a>
	<a href='<?=$about?>'>
		<p class='site-slogan'><?=get_bloginfo('description')?></p>
	</a>
	<!-- @TODO add menu displaying items and search bar in new line as a block -->
</header>