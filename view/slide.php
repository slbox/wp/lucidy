<?php
namespace Lucidy;
loadLib('EntryPost');
$slide = new Entry\Post();
?>

<figure
	id=slideshow-slide-<?=$slide->id?>
	class='slideshow-slide box--shadow-sharp'
	tabindex='<?='x'?>'
>
	<a href='<?=$slide->url?>'>
		<img
			src='<?=$slide->thumb['src']?>'
			alt='<?=$slide->thumb['caption']?>'
			class='slideshow-slide-thumbnail'
		/>
		<div class='slideshow-slide-glass glass'></div>
		<figcaption class='slideshow-slide-info btn'>
			<h4 class='slideshow-slide-title'>
				<?=$slide->title?>
			</h4>
			<time class='slideshow-slide-date'>
				<?=$slide->date?>
			</time>
		</figcaption>
	</a>
</figure>