<?php
Lucidy\loadLib('Query');

$defInfo = [
	'type'	=> 'post',
	'form'	=> 'conveyor'
];
$i = Lucidy\get($d, 'info', $defInfo);

$slides = Lucidy\get($d, 'query', []);
$query = new Lucidy\Query($slides);

$slideNav = [];
?>

<?php if ( $query->have_posts() ): ?>
	<figure class='slideshow slideshow--<?=$i['form']?>'>
		<div class='slideshow-box'>
			<?php $query->render('slide'); ?>
		</div>
		<nav class='slideshow-nav'>
			<?php $query->new($slideNav)->render('slideNav') ?>
		</nav>
	</figure>
<?php endif ?>