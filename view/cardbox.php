<?php
Lucidy\loadLib('Query');

$defInfo = [
	'type'	=> 'post', # TODO upgrade to able change type programtically
	'form'	=> 'normal'
];
$i = Lucidy\get($d, 'info', $defInfo);

$cards = Lucidy\get($d, 'query', []);
$query = new Lucidy\Query($cards);
?>

<?php if ( $query->have_posts() ): ?>
	<section class='cardbox <?=join(" ", $i)?>'>
		<?php $query->render('card', $d) ?>
		<a
			class='btn placeholder'
			data-pagi-cur='<?=$query->pagi()?>'
		>
			<!-- TODO need localization support -->
			Click to load more
		</a>
	</section>
<?php else: ?>
	<div class='notice'>
		<p><?=Lucidy\noticeMsg($i['type'])?></p>
	</div>
<?php endif ?>