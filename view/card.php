<?php
namespace Lucidy;
loadLib('EntryPost');
$card = new Entry\Post();
?>

<a
	id='cardbox-card-<?=$card->id?>'
	class='cardbox-card cardbox-card--big box'
	href='<?=$card->url?>'
>
	<figure class='cardbox-card-thumbnail'>
		<img
			src='<?=$card->thumb['src']?>'
			alt='<?=$card->thumb['caption']?>'
			class='cardbox-card-img'
		/>
	</figure>
	<article class='cardbox-card-body'>
		<h6 class='cardbox-card-title'>
			<?=$card->title?>
		</h6>
		<div class='cardbox-card-content'>
			<?=$card->excerpt?>
		</div>
		<time
			class='cardbox-card-date'
			datetime='<?=$card->machineTime?>'
		>
			<?=$card->date?>
		</time>
	</article>
</a>