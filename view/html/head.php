<!DOCTYPE html>
<html lang='<?=get_bloginfo('language')?>'>
<head>
	<meta charset='<?=get_bloginfo('charset')?>'>
	<meta name='viewport' content='width=device-width, initial-scale=1.0'>
	<meta http-equiv='X-UA-Compatible' content='ie=edge'>
	<?php Lucidy\render('html/meta') ?>
	<?php wp_head() ?>
</head>
<body id='Luros' <?php body_class() ?> >