	<aside id='Bulletin'>
		<ol class='bulletin-list' data-limit='3'></ol>
		<template id='BulletinItem'>
			<li class='bulletin-item bulletin-item--show box'>
				<p class='bulletin-msg'></p>
				<button class='bulletin-close btn'>×</button>
				<!-- TODO which one? × or `hide` -->
			</li>
		</template>
	</aside>
	<a id='UP' class='btn ico invisible'>up</a>
	<?php wp_footer() ?>
</body>
</html>