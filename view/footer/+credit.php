<?php if ( get_theme_mod('theme_credit') ): ?>
	<p class='theme-credit'>
		Theme design © 
		<a href='https://ngdangtu.com'>Đăng Tú</a>
	</p>
<?php endif ?>