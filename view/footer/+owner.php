<?php
$owner = new WP_User( get_theme_mod('blog_owner') )
?>

<?php if ( $owner->exists() ): ?>
	<p class='blog-owner'>
		<?=lutra('Website owner', 'Blog owner in website footer, a noun phrase tailing by a colon')?>: 
		<a rel='author' href='<?=get_home_url(null, "author/{$owner->user_login}")?>'>
			<?=$owner->display_name?>
		</a>
	</p>
<?php endif ?>