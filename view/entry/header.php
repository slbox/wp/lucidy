<?php $entry = $d['entry'] ?>

<header class='entry-header'>
	<h1 class='entry-title'>
		<?=$entry->title?>
	</h1>
	<figure class='entry-cover'>
		<img
			class='entry-cover-img'
			src='<?=$entry->cover['src']?>'
			alt='<?=$entry->cover['caption']?>'
		/>
		<figcaption class='entry-cover-caption'>
			<?=$entry->cover['caption']?>
		</figcaption>
	</figure>
</header>