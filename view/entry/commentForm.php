<?php
namespace Lucidy;
loadLib('User');
loadLib('CmtResponse');
// take a param to decide which msg reply to
$cmt = new Cmt\Response();
$user = new \WP_User();
$translatedContext = 'Comment Form';
?>

<!-- TODO change wp-comments-post file to prevent spambot -->
<form
	action='post'
	class='comment-form'
	method='<?=site_url('wp-comments-post.php')?>'
>
	<input name='comment_post_ID' value='<?=$d['entryID']?>' type='hidden'/>
	<input name='comment_parent'  value='<?=($cmt->id ?: 0)?>' type='hidden'/>
	
	<?php if ( $user ): ?>
		<input name='email' value='<?=$user->email?>' type='hidden'/>
		<input name='author' value='<?=$user->display_name?>' type='hidden'/>
		<input name='url' value='<?=$user->url?>' type='hidden'/>
	<?php else: ?>
		<label>
			<p><?=lutra('Email', $translatedContext)?></p>
			<input name='email' type='email' value=''/>
		</label>
		<label>
			<p><?=lutra('Name', $translatedContext)?></p>
			<input name='author' type='text' value=''/>
		</label>
		<label>
			<p><?=lutra('Website', $translatedContext)?></p>
			<input name='url' type='url' value=''/>
		</label>
	<?php endif ?>

	<label>
		<!-- TODO -->
		<p><?=lutra('Comment', $translatedContext)?></p>
		<textarea
			placeholder='<?=lutra('Leave your comment here', $translatedContext)?>'
			name='comment' 
		></textarea>
	</label>

	<button type='submit' name='Submit'>
		<?=lutra('Send', $translatedContext)?>
	</button>
	<button type='reset'>
		<?=lutra('Cancel', $translatedContext)?>
	</button>
</form>