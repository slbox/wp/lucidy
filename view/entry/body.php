<?php $entry = $d['entry'] ?>

<article
	<?php if ($entry->isProtected): ?>
		id='entry-request-pwd'
	<?php else: ?>
		id='entry-<?=$entry->id?>'
	<?php endif; ?>
	class='entry-content stretch-box'
>
	<div class='entry-meta entry-body-top'>
		<!-- placeholder auto reading machine -->
		<time
			title='<?=""?>'
			class='entry-published-time btn'
			datetime='<?=$entry->machineTime?>'
			data-shortlink='<?=$entry->lnk?>'
		> <!-- auto copy link onclick and announce back on screen -->
			<?=$entry->date?>
		</time>
	</div>
	<!-- <section class='entry-toc'>for future: series feature</section> -->
	<section class='entry-body'>
		<?php Lucidy\render('entry/bodyContent', $entry->content) ?>
	</section>
	<div class='entry-meta entry-body-bottom'>
		<div class='entry-share'></div>
		<div class='entry-catalog'></div>
		<div class='entry-tags'></div>
	</div>
</article>