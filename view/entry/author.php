<?php $author = $d['author'] ?>

<aside class='entry-author box'>
	<article class='entry-author-info'>
		<small class='entry-author-boxLabel'>
			<?=lutra(
				'About author',
				'Title of author card in singular template.'
			)?>
		</small>
		<h2 class='entry-author-name'>
			<a
				rel='author'
				href='<?=get_home_url(null, "author/{$author->user_login}")?>'
			><?=$author->display_name?></a>
		</h2>
		<div class='entry-author-bio'>
			<?=$author->description?>
		</div>
		<address class='entry-author-social'>
			<!-- author social network account -->
		</address>
	</article>
	<figure class='entry-author-avatar'>
		<!-- <img src='' alt="<?=$author->display_name?>'s portrait"/> -->
		<?=get_avatar($author->id, 128, '', $author->display_name)?>
	</figure>
</aside>