<?php $content = $d ?>
<?php if ( is_array($content) ): ?>
	<nav class='entry-body-pagination'></nav>
	<?php foreach ($content as $pgPos => $page): ?>
		<section
			id='pg<?=($pgPos + 1)?>'
			class='entry-body-page <?=($pgPos != 0 ? 'invisible' : '')?>'
		><?=$page?></section>
	<?php endforeach; ?>
	<nav class='entry-body-pagination'></nav>
<?php else: ?>
	<?=$content?>
<?php endif; ?>