<?php if ( is_active_sidebar($widget->id) ): ?>
	<aside
		id='widget-<?=$widget->id?>'
		class='widget <?=$widget->type?>'
	>
		<div class='widget-title'>
			<?=$widget->title?>
		</div>
		<div class='widget-area'>
			<?php?>
		</div>
	</aside>
<?php endif; ?>