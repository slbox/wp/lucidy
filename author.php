<?php
namespace Lucidy;
$user = new \WP_User();
?>

<main>
	<?php
	debug( $user );
	debug( get_query_var('author') );
	debug( get_query_var('author_name') );
	
	render(
		'profile/bio',
		['user' => $user]
	);
	render(
		'profile/articles',
		['entries' => $user]
	)
	?>
</main>

<?php footer('detail') ?>