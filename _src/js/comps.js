class Button {
	constructor(id) {
		this.core = document.querySelector(id)
	}

	toggle(active) {
		const toggle = 'invisible'

		if (active)
			this.core.classList.remove(toggle)
		else
			this.core.classList.add(toggle)
	}

	onclick() { }
}

class btnUP extends Button {
	constructor() {
		super('#UP')
	}

	goUp(e) {
		e.preventDefault()
		window.scrollTo(0, 0)
	}

	onclick() {
		this.core.addEventListener('click', this.goUp)
	}
}

class btnMagnifier extends Button {
	constructor() {
		super('#UP')
	}
}



export const UP = new btnUP()
export const magnifier = new btnMagnifier()

export class btnRest extends Button {}