const url = window.location

export class URL {
	static set origin(x) {
		console.error('It is impossible to modify URL.origin property.')
	}
	static get origin() {
		return `${url.protocol}//${url.hostname}`
	}

	static get isUgly(x) { }

	static get isUgly() {
		const ugly = url.search.indexOf('?p=') != -1
		return ugly ? true : false
	}

	static x(...params) { }
}