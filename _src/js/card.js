export class Card {
	constructor(card) {
		if (typeof card === 'string') {
			this.name = card
			this.self = document.querySelector(card)
		} else if (card instanceof HTMLElement) {
			this.self = card
			this.name = this.primeClass(card.className)
		}

		this.id = this.self.id
		this.thumb = this.child('thumbnail')
		this.body = this.child('body')
	}

	child(el) {
		return this.self.querySelector(`.${this.name}-${el}`)
	}

	/**
	 * Get the main class of the card
	 * @param {String} lsClass from Element.className
	 */
	primeClass(lsClass = '') {
		// TODO optimize the case 'class class-1    class-1-prop'
		const list = lsClass.trim().split(' ')

		for (const item of list) {
			if (item.indexOf('--') === -1) return item
		}
	}

	fixSize() {
		// TOFIX do not resize small size (--small) or small viewport
		const verticalMarginOf = el => {
			const style = window.getComputedStyle(el)
			const top = parseFloat(style.marginTop)
			const bottom = parseFloat(style.marginBottom)
			return (top + bottom)
		}

		const fullHeightOf = name => {
			const el = this.child(name)
			return parseFloat(verticalMarginOf(el) + el.clientHeight)
		}

		const title = fullHeightOf('title')
		const content = fullHeightOf('content')
		const date = fullHeightOf('date')

		const bodyMargin = verticalMarginOf(this.body)

		this.self.style.height = `${title + content + date + bodyMargin}px`
	}
}

