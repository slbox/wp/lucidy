import { UP } from './comps';
import { Card } from './card';

const displayUPbtn = () => {
	const cond = window.scrollY > window.innerHeight
	UP.toggle(cond)
}
UP.onclick()
window.addEventListener('scroll', displayUPbtn)


const adjustCardSize = () => {
	const cards = document.querySelectorAll('.cardbox-card')
	cards.forEach(card => {
		new Card(card).fixSize()
	})
}
window.addEventListener('resize', adjustCardSize)


const getRestData = () => {}
window.addEventListener('click', getRestData)

window.addEventListener('load', () => {
	displayUPbtn()
	adjustCardSize()
})