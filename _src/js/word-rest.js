//localhost/wp/wp-json/wp/v2/taxonomies
import { URL } from './url'

class Router {
	constructor() {
		this.root = URL.isUgly ? '?rest_route=' : 'wp-json'
		this.wp = 'wp/v2'
	}

	join() {
		return [
			URL.origin,
			this.root,
			this.wp
		].join('/')
	}
}

/**
 * Read only API
 */
class WordRest {
	constructor() {
		this.init()
	}

	init() {
		this.GET = 'GET'
		this.router = new Router()
	}
}

class WR_Pagination {
	constructor() { }

	load() { }

	prev() { }

	next() { }

	hasMore() { }
}