const isDev = process.env.NODE_ENV === 'developement'
const { src, dest, series, watch, parallel } = require('gulp')


/* Source & Destination */
const path = require('path')
const input = {}
const output = path.join(__dirname, '..', 'asset')

input.style = path.join(__dirname, 'css')
input.sss = [
	path.join(input.style, 'lucidy.sss'),
	path.join(input.style, 'fonts.sss')
]

input.script = path.join(__dirname, 'js')
input.js = path.join(input.script, 'main.js')

const bundle = path.join(output, 'bundle.js')


/* Tasks */
const style = () => {
	const pcss = require('gulp-postcss')
	const rename = require('gulp-rename')

	return src(input.sss, {sourcemaps: isDev})
		.pipe(pcss())
		.pipe(rename(path => {
			path.extname = '.css'
		}))
		.pipe(dest(output, {sourcemaps: '.'}))
}

const script = () => {
	const pkg = require('rollup')
	const resolve = require('rollup-plugin-node-resolve')
	const eslint = require('rollup-plugin-eslint').eslint
	const babel = require('rollup-plugin-babel')

	const rollin = {
		input: input.js,
		cache: true
	}
	const rollout = {
		file: bundle,
		format: 'iife',
		name: 'lucidyPkg',
		compact: !isDev,
		sourcemap: isDev
	}
	const rollcheck = {
		throwOnError: true,
		// formatter: 'html',
		report: { // For future
			file: 'eslint-report',
			path: path.join(__dirname, '..', '_logs'),
			formatter: 'html'
		}
	}

	rollin.plugins = [
		resolve(),
		eslint(rollcheck),
		babel()
	]

	return pkg.rollup(rollin)
		.then(pkg => pkg.write(rollout))
}

const clean = () => {
	const del = require('del')

	const trash = [
		bundle,
		path.join(output, '*.css'),
		path.join(output, '*.map'),
	]
	const options = { force: true }

	return del(trash, options)
}


/* Define tasks for CLI */
exports.x = style
exports.css = () => { watch(input.style, style) }
exports.js = () => { watch(input.script, script) }
exports.clean = clean
exports.default = series(clean, parallel(style, script))