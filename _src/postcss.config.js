const path = require('path')
const res = {
	mix: path.join(__dirname, 'css', 'pcss', 'mixin'),
	func: path.join(__dirname, 'css', 'pcss', 'func', '*.js'),
	var: path.join(__dirname, 'css', 'pcss', 'data')
}

const config = {}

config.import = {
	prefix: '+',
	extensions: '.sss'
}

config.mixins = {
	mixinsDir: res.mix
}

config.function = {
	glob: res.func
}

config.var = {
	globals: require(res.var).default
}

config.nano = {
	preset: [
		'default',
		{ cssDeclarationSorter: true }
	]
}

module.exports = {
	parser: 'sugarss',
	plugins: {
		'postcss-easy-import': config.import,
		'postcss-mixins': config.mixins,
		'postcss-nested': {},
		'postcss-functions': config.function,
		'postcss-variables': config.var,
		'postcss-color-function': {},
		'cssnano': config.nano
	}
}