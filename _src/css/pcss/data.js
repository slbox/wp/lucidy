const breakpoint = {
	viewport: {
		width: {
			tv: '',
			pc: '',
			tablet: '',
			phone: '',
		},
		height: {
			tv: '',
			pc: '',
			tablet: '',
			phone: ''
		}
	},
	card: {
		max: '750px',
		med: '400px',
		min: '330px'
	}
}

const font = {
	icon: 'Lucion',
	header: 'Itim, cursive',
	body: 'Mali, cursive',
	size: '12pt'
}

const viewportWidth = {
	small: '360px',
	medium: '960px'
}

const _breakpoint = {
	max: '750px',
	min: '330px',
	square: '300px'
}

const shadow = {
	soft: '0 3px 3px #0f0f0f1c',
	sharp: '0 2px 1px #0f0f0f6b'
}

const tex = name => `img/${name}.png`
const texture = {
	glass: tex('AeroGlass'),
	bg: tex('Leaves')
}

const scheme = {
	Luros: { // LUcid ROSe
		name: 'Luros',
		txt: '#090909',
		accent: '#ffbbd7',
		fg: '#fafafa',
		bg: '#feeff4',
		new: '#fd82b1',
		old: '#e04386'
	}
}

exports.font = font

exports.meta = {
	font: {
		family: {
			'itim': {
				name: ['Mali Light', 'Mali-Light'],
				unicode: {
					latin: 'itim/v4/0nknC9ziJOYe8ANAkA',
					latinExt: 'itim/v4/0nknC9ziJOYe8A1AkP7Z',
					vi: 'itim/v4/0nknC9ziJOYe8AxAkP7Z'
				}
			},
			'mali': {
				name: ['Itim', 'Itim-Regular'],
				unicode: {
					latin: 'mali/v3/N0bV2SRONuN4QIbmGlNQJA',
					latinExt: 'mali/v3/N0bV2SRONuN4QIbmGl1QJObW',
					vi: 'mali/v3/N0bV2SRONuN4QIbmGlxQJObW'
				}
			}
		},
		unicode: {
			latin: 'U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD',
			latinExt: 'U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF',
			vi: 'U+0102-0103, U+0110-0111, U+1EA0-1EF9, U+20AB'
		}
	}
}

exports.breakpoint = breakpoint

exports.default = {
	f: font,
	vpw: viewportWidth,

	card: _breakpoint, //TODO change card to breakpoint or brp
	shadow: shadow,

	tex: texture,

	/***
	 * Color schemes
	 */
	scheme: scheme.Luros
}