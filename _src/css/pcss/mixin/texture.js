module.exports = (mixin, texture) => {
	mixin.replaceWith(`
		background-image: url(${texture});
		background-repeat: repeat;
		background-position: center;
	`)
}