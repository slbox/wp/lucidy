const size = require('../data').default.card

/**
 * TODO fix this super dirty code
 */
const nhanPx = (a, b) => {
	const newA = parseFloat(a)
	const newB = parseFloat(b)
	return `${newA * newB}px`;
}

module.exports = (mixin, up = 1) => {
	mixin.replaceWith(`
		max-width: ${nhanPx(size.max, up)};
		min-width: ${nhanPx(size.min, up)};
		width: 100%;
	`)
}