const decl = (prop, val) => (val === 'null' ? '' : `${prop}: ${val};`)

const column = val => {
	const dDecl = val => decl(
		'flex-direction',
		val == 'col' ? 'column' : val
	)

	if (val.has('rev')) {
		if (val.has('col')) {
			return dDecl('column-reverse')
		} else {
			return dDecl('row-reverse')
		}
	} else {
		return dDecl(val)
	}
}

/**
 * `null` value remove the whole declaration.
 * 
 * @howto
 * `@mixin tile <column:c>, <gap:g>`
 * 
 * @example
 * - `@mixin tile` -> set 1 col, set 10px gap
 * - `@mixin tile 5` -> set 5 col, set 10px gap
 * - `@mixin tile 2, 15` -> set 2 col, set 15px gap
 */
module.exports = (mixin, c = 1, g = 10) => {
	mixin.replaceWith(`
		display: grid;
		grid-gap: ${g}px;
		grid-auto-rows: grid;
		${col(c)}
	`)
}