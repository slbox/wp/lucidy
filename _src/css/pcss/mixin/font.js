const meta = require('../data').meta.font
const range = meta.unicode

const google = (id) => {
	const file = id.replace(/^\/+|\/+$/g, '')
	return `url(https://fonts.gstatic.com/s/${file}.woff2) format('woff2')`
}

const src = () => {
	const stack = []
}

const unicode = (uCode = 'latin') => {
	const val = range[uCode]

	try {
		if (!val) {
			throw `${uCode} range is missing! You can update it in data.js file.`
		}
	} catch (e) {
		console.error(`UnicodeRange: ${e}`)
	}

	return `unicode-range: ${val};`
}

const fontface = (font, uCode = 'latin', style = 'normal', weight = 400) => {
	return `
		@font-face {
			font-family: ${font.name[0]};
			font-style: ${style};
			font-weight: ${weight};
			font-display: swap;
			${font.src(uCode)}
			${unicode(uCode)}
		}
	`
}

const fontfaces = font => {
	for (const key in range) {
		if (range.hasOwnProperty(key)) {
			const uCode = range[key];

		}
	}
}

/**
 * @howto
 * `@mixin font <id>, <range>`
 * 
 * @example
 * - `@mixin font itim` -> Add Itim fontface with all
 * 	available ranges in `css/pcss/data.js`
 */
module.exports = (mixin, id, range = 'all') => {
	const font = meta.family[id]

	try {
		if (!font) throw 'If this is a mistake, update new font in data.js file.';
	} catch (e) {
		console.error(`Font: ${e}`)
	}

	if (range === 'all') {
		// 
	} else if (range.indexOf(' ') != -1) {
		// 
	} else {
		mixin.replaceWith(fontface(font, range))
	}
}