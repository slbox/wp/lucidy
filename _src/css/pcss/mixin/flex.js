const decl = (prop, val) => (val === 'null' ? '' : `${prop}: ${val};`)

String.prototype.has = function (el) {
	return this.indexOf(el) == -1 ? false : true
}

/**
 * Add flex-wrap declaration
 * 
 * Accepted values:
 * - no | yes | rev
 * - orginal
 */
const wrap = val => {
	const wDecl = val => decl('flex-wrap', val)

	switch (val) {
		case 'yes':
			return wDecl('wrap')

		case 'no':
			return wDecl('nowrap')

		case 'rev':
			return wDecl('wrap-reverse')

		default:
			return wDecl(val)
	}
}

/**
 * Add flex-direction declaration
 * 
 * Accepted values:
 * - col | col-rev | rev-col
 * - row | row-rev | rev-row
 * - orginal
 */
const direction = val => {
	const dDecl = val => decl(
		'flex-direction',
		val == 'col' ? 'column' : val
	)

	if (val.has('rev')) {
		if (val.has('col')) {
			return dDecl('column-reverse')
		} else {
			return dDecl('row-reverse')
		}
	} else {
		return dDecl(val)
	}
}

/**
 * `null` value remove the whole declaration.
 * 
 * @howto
 * `@mixin flex <direction:d>, <wrap:w>`
 * 
 * @example
 * - `@mixin flex null, yes` -> remove direction, add wrap value
 * - `@mixin flex col` -> add column direction only
 */
module.exports = (mixin, d = 'null', w = 'null') => {
	const flex = 'display: flex;' +
		direction(d) +
		wrap(w);

	mixin.replaceWith(flex)
}