const decl = require('../tools').decl

const prefixes = ['ms', 'moz', 'webkit', null]

const select = (pre, val) => {
	const prefix = pre != null ?
		`-${pre}-user-select` :
		'user-select'
	return decl(prefix, val)
}

module.exports = (mixin, val = 'none') => {
	const decls = prefixes.map(
		pre => select(pre, val)
	).join('')
	mixin.replaceWith(decls)
}