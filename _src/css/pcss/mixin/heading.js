const fontname = require('../data').font.header

const fontsize = (lvl, size) => {
	return `
		h${lvl} {
			font-size: ${size};
		}
	`
}

const common = (margin = 0) => {
	const set = [1, 2, 3, 4, 5, 6].map(inx => `h${inx}`)
	return `
		${set.join(', ')} {
			font-family: ${fontname};
			font-weight: normal;
			line-height: 1;
			margin: ${margin};
		}
	`
}

/**
 * @howto
 * `@mixin flex <minSize>, <margin>, <step>, <unit>`
 * 
 * @example
 * - `@mixin heading 13, 2, px` -> Add a heading set
 * 	with fontsize of h6 is 13px and h5 is 15px
 */
module.exports = (mixin, minSize, margin = 0, step = 2, unit = 'pt') => {
	let headings = ''
	minSize = new Number(minSize)
	step = new Number(step)

	for (let lvl = 6; lvl >= 1; lvl--) {
		headings += fontsize(lvl, `${minSize}${unit}`)
		minSize += step
	}
	headings += common(margin)

	mixin.replaceWith(headings)
}