<?php
namespace Lucidy\Entry;

// FiXME fail to load empty content post!

class Entry {
	public		$date			= null;
	public		$machineTime	= null;
	protected	$id				= null;
	private		$ContentDOM		= null;

	public function id() {
		return $this->id;
	}

	private function thePost() {
		return get_post($this->id);
	}

	protected function avatar($size) {
		if ( has_post_thumbnail($this->id) ):
			return [
				'src'		=> get_the_post_thumbnail_url($this->id, $size),
				'caption'	=> get_the_post_thumbnail_caption($this->id)
			];
		else:
			return [
				'src'		=> \Lucidy\defaultThumb($size),
				'caption'	=> ''
			];
		endif;
	}

	public function isProtected() {
		return post_password_required($this->id);
	}

	public function isPrivate() {
		$_post = $this->thePost();
		return ('private' == $_post->post_status);
	}

	public function url() {
		return \get_permalink($this->id);
	}

	public function lnk() {
		return \wp_get_shortlink($this->id);
	}

	public function title() {
		$_post = $this->thePost();
		$_title = $_post->post_title;
		return \apply_filters( 'the_title', $_title, $this->id );
	}

	public function thumb() {
		return $this->avatar('thumbnail');
	}

	public function excerpt() {
		if ( \has_excerpt($this->id) ):
			return \get_the_excerpt($this->id);
		elseif ( $this->ContentDOM->hasBreakTag() ):
			return $this->ContentDOM->rawMore;
		else:
			$_words = \get_theme_mod('max_excerpt_words');
			return $this->ContentDOM->text($_words);
		endif;
	}

	public function cover() {
		return $this->avatar('cover');
	}

	public function content() {
		$_content = $this->ContentDOM->raw;
		$_sect = \preg_split('/<!--nextpage-->/i', $_content);
		return \preg_replace(
			'/<!--(.*)-->/i', 
			'', 
			(\count($_sect) === 1 ? $_sect[0] : $_sect)
		);
	}

	public function catalog() {
		# TODO
	}

	public function tags() {
		# TODO
	}

	public function author() {
		$_userID = (int) \WP_Post::get_instance($this->id)->post_author;
		return new \WP_User($_userID);
	}

	protected function init() {
		$this->date = \get_the_date('', $this->id);
		$this->machineTime = \get_the_date('c', $this->id);
		$this->ContentDOM = new ContentDOM($this->id);
	}

	public function __get($name) {
		if ( \method_exists($this, $name) ):
			return $this->$name();
		else:
			$_className = static::class;
			throw new \Exception("Class $_className doesn't have any property nor method named ${name}.");
		endif;
	}

	public function __set($name, $value) {
		throw new \Exception("The $name property cannot be set.");
	}

	function __construct($id = null) {
		$this->id = $id != null ?: \get_the_ID();

		if (!$this->id):
			throw new \Exception('The global $post has not been set yet. This object lives in the WP loop. And make sure WP_Query->the_post() is called.');
		endif;

		$this->init();
	}
}

class ContentDOM {
	public		$id			= 0;
	private		$raw		= null;
	private		$rawMore	= null;
	private		$DOM		= null;

	public function __get($name) {
		return $this->$name;
	}

	function __construct($id) {
		$this->id = $id;
		$this->raw = post_password_required($id) ?
			lutra(
				"<p>Oh no! This article wasn't mean for publicity.</p>",
				'In case the protected content was displayed in public'
			) :
			apply_filters(
				'the_content',
				\WP_Post::get_instance($id)->post_content
			);

		if ($this->raw == '')
			$this->raw = lutra(
				'<p>This post has no content</p>',
				'If content is empty, this line will be used instead.'
			);
		
		$_rawDOM = ContentDOM::encoding($this->raw);

		$this->DOM = new \DomDocument();
		if ( !@$this->DOM->loadHTML($_rawDOM) ):
			throw new \Exception('Parsing raw content to DOM failed!');
		endif;
	}

	static protected function encoding(string $raw) {
		# Convert the content to correct charset
		return \mb_convert_encoding(
			$raw,
			'HTML-ENTITIES',
			\get_bloginfo('charset')
		);
	}

	static protected function cleanContent(\DOMNode $node):string {
		$_childLs = $node->childNodes;
		foreach ($_childLs as $_child):
			if (
				# Prevent <a> existence
				$_child instanceof \DOMElement &&
				$_child->tagName == 'a'
			):
				$_txt = new \DOMText($_child->textContent);
				$node->replaceChild($_txt, $_child);
			endif;
		endforeach;

		return $node->ownerDocument->saveHTML($node);
	}

	protected function getTags(string $tag):\DOMNodeList {
		return $this->DOM->getElementsByTagName($tag);
	}

	protected function getTag(string $tag, int $index = 0) {
		if ($index < 0) $index = 0;
		return $this->getTags($tag)->item($index);
	}

	protected function tag(string $tag, $truthy, $falsy = null) {
		$el = $this->getTag($tag);
		if (is_callable($truthy) && $el):
			return $truthy($el);
		elseif ($el):
			throw new \Exception("Found $tag tag but no callback function to pass into.");
		endif;

		if ( is_callable($falsy) ):
			return $falsy();
		else:
			return null;
		endif;
	}

	public function video() {
		return $this->tag(
			'video',
			function($vid) {
				return $vid->getAttribute('src');
			}
		);
	}

	public function poster() {
		return $this->tag(
			'video',
			function($vid) {
				return $vid->getAttribute('poster');
			}
		);
	}

	public function photo() {
		return $this->tag(
			'img',
			function($img) {
				return [
					'src' => $img->getAttribute('src'),
					'caption' => $img->getAttribute('alt')
				];
			}
		);
	}

	public function text(int $maxWords = 50):string {
		return $this->tag(
			'p',
			function($p) use ($maxWords) {
				$_all = \explode(' ', $p->nodeValue);
				$limCondition = \count($_all) > $maxWords;
				$_words = $limCondition ?
					\array_slice($_all, 0, $maxWords) :
					$_all;

				return \join(' ', $_words).($limCondition ? '…' : '');
			},
			function() {return '';}
		);
	}

	public function hasBreakTag():bool {
		$_sect = \explode('<!--more-->', $this->raw);

		if (\count($_sect) <= 1):
			return false;
		else:
			$_breakDOM = new \DomDocument();
			$_beforeMore = ContentDOM::encoding($_sect[0]);
			if ( @$_breakDOM->loadHTML($_beforeMore) ):
				foreach ($_breakDOM->getElementsByTagName('p') as $_node):
					$this->rawMore .= ContentDOM::cleanContent($_node);
				endforeach;
				return $this->rawMore ? true : false;
			else:
				return false;
			endif;
		endif;
	}
}