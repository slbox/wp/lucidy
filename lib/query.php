<?php
namespace Lucidy;

class Query extends \WP_Query {
	/**
	 * Query WP posts with a customized config
	 *
 	 * @param array $config	Configuration list
	 */
	function __construct($config=[]) {
		$query = $this->mergeQuery(is_array($config) ? $config : []);
		parent::__construct($query);
	}

	protected function mergeQuery(array $config=[]):array {
		$default = [
			'post_status'			=> ['publish', 'private'],
			'post_type'				=> 'post',
			'has_password'			=> false,
			'ignore_sticky_posts'	=> true,
			'paged'					=> get_query_var('paged') ?: 1,
			'perm'					=> 'readable'
		];
		return array_replace($default, $config);
	}

	public function render(string $item, array $data=[]):void {
		while( $this->have_posts() ):
			$this->the_post();
			render($item, $data);
		endwhile; wp_reset_postdata();
		# $query->the_post() adds the current post
		#	from the loop to global $post variable
	}

	public function new(array $config):Query {
		$newQuery = $this->mergeQuery($config);
		$this->query($newQuery);
		return $this;
	}

	public function pagi($default = 1) {
		// TOFIX didn't work
		$type = is_front_page() ? 'page' : 'paged';
		return $this->get($type, $default);
	}
}


function noticeMsg($type):string {
	return lutra(
		"Sorry, no ${type} matched your criteria.",
		'Notice message of no available post'
	);
}